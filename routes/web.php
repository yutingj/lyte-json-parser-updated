<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//
//Route::resource('products', 'ProductController');
//
//Route::resource('clients', 'ClientController');
//
//
Route::resource('requests', 'RequestController');
//
//Route::get('download/{id}', 'DownloadPDFController@download')->name("download");
Route::get('updateEmpty', 'RequestController@updateEmpty')->name("updateEmpty");
Route::get('charts', 'RequestController@charts')->name("charts");

Route::resource('parseJson', 'ParseJsonController');
Route::get('/', 'HomeController@index')->name("index");




