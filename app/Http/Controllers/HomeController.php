<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    //

    public function index()
    {
        $data['title'] = 'Welcome back';
        return view('home.index',$data);
    }
}
