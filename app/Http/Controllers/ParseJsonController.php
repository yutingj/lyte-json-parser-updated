<?php

namespace App\Http\Controllers;

use App\Qeery;
use App\Request as RequestModel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use \Datetime;
use Illuminate\Http\Request;

class ParseJsonController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Parse Json';
        return view('parseJson.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Parse Json';
        return view('parseJson.create', $data);
    }

    public function validateDate($date, $format = 'M d, Y'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = json_decode($request->input("jsonString"), true);
        foreach ($array as $key => $value) {
            $requestID = $value["#debug"]["requestId"];
            $url = $value["#debug"]["url"];
            $method = $value["#debug"]["method"];
            $retryCount = $value["#debug"]["retryCount"];
            $statusCode = $value["#debug"]["statusCode"];
            $durationSecs = $value["#debug"]["durationSecs"];
            $hasNextPage = $value["hasNextPage"];
            $resultsTotal = $value["resultsTotal"];

            RequestModel::create(
                [
                    "id" => $requestID,
                    "url" => $url,
                    "method" => $method,
                    "retryCount" => $retryCount,
                    "statusCode" => $statusCode,
                    "durationSecs" => $durationSecs,
                    "hasNextPage" => $hasNextPage,
                    "resultsTotal" => $resultsTotal,
                ]
            );

            if (isset($value["relatedQueries"])) {
                foreach ($value["relatedQueries"] as $key => $val) {
                    if (isset($val['title']) && isset($val['url'])) {
                        DB::table('related_queries')->insert([
                            "title" => $val['title'],
                            "url" => $val['url'],
                            "requestID" => $requestID,
                            "created_at" => Carbon::now()->toDateTimeString(),
                            "updated_at" => Carbon::now()->toDateTimeString(),
                        ]);
                    }
                }
            }

            if (isset($value["searchQuery"])) {
                    $val = $value["searchQuery"];
                    $data['test'] = $val;
                        DB::table('search_queries')->insert([
                            "term" => $val['term'],
                            "device" => $val['device'],
                            "requestID" => $requestID,
                            "page" => $val['page'],
                            "device" => $val['device'],
                            "type" => $val['type'],
                            "domain" => $val['domain'],
                            "countryCode" => $val['countryCode'],
                            "locationUule" => $val['locationUule'],
                            "resultsPerPage" => $val["resultsPerPage"],
                            "created_at" => Carbon::now()->toDateTimeString(),
                            "updated_at" => Carbon::now()->toDateTimeString(),
                        ]);
            }

            if (isset($value["organicResults"])) {
                foreach ($value["organicResults"] as $key => $vala) {
                    if (isset($vala["title"]) && isset($vala["url"])) {
                        if (strpos($vala['description'], "-") !== false) {
                            $dateString = substr($vala["description"], 0, strpos($vala['description'], "-") - 1);
                            if ($this->validateDate($dateString)) {
                                DB::table('results')->insert([
                                    "title" => $vala['title'],
                                    "displayURL" => $vala['displayedUrl'],
                                    "description" => $vala['description'],
                                    "companyName" => "",
                                    "date" => Carbon::parse($dateString),
                                    "requestID" => $requestID,
                                    "created_at" => Carbon::now()->toDateTimeString(),
                                    "updated_at" => Carbon::now()->toDateTimeString()
                                ]);
                            } else {
                                DB::table('results')->insert([
                                    "title" => $vala['title'],
                                    "displayURL" => $vala['displayedUrl'],
                                    "description" => $vala['description'],
                                    "companyName" => "",
                                    "date" => null,
                                    "requestID" => $requestID,
                                    "created_at" => Carbon::now()->toDateTimeString(),
                                    "updated_at" => Carbon::now()->toDateTimeString()
                                ]);
                            }

                        }
                    }

                }
            }
        }

        $data['title'] = 'Parse Json Test';
//        $data['test'] = json_decode($request->input("jsonString"), true);
        return view('parseJson.test', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }





}
