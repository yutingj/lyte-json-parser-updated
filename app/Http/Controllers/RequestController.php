<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Result;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Date;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Requests";
        $data['requests'] = DB::table('requests')
            ->orderBy('created_at','desc')
            ->paginate(10);
        return view("requests.index ",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function updateEmpty() {
        $data['title'] = "Requests";
        $data['result'] = DB::table('results')
            ->orderBy('created_at','desc')
            ->where("companyName","=","")
            ->first();
        $data['count'] = DB::table('results')
            ->orderBy('created_at','desc')
            ->where("companyName","=","")
            ->count();
        return view("results.edit ",$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function charts() {
        $data['title'] = 'Charts';

        $data['test'] = DB::table('results')
            ->join('requests', 'requests.id', '=', 'results.requestID')
                ->join('search_queries', 'results.requestID', '=', 'search_queries.requestID')
            ->select("companyName", DB::raw('COUNT(results.id) as count')
                ,DB::raw("DATE_FORMAT(date, '%Y%m') as yearmonth"),DB::raw('search_queries.term as term'))
            ->groupBy("companyName",
                DB::raw("DATE_FORMAT(date, '%Y%m')"),"search_queries.term")
            ->orderBy("date","asc")
            ->get();
        $finaleArray = array();
        $x = 0;
        $typeofQueries = DB::table( 'search_queries')->select('term')->distinct()->get();
            foreach($typeofQueries as $query) {
                $newArray = array();
                foreach($data['test'] as $record) {
                    if (strcmp($record->term,$query->term) == 0) {
                        if (isset($record->yearmonth)) {
                            $date = Carbon::createFromDate(substr($record->yearmonth,0,4),
                                    substr($record->yearmonth,4,6), 1)->getTimestamp()*1000;
                            array_push($newArray,array("x" => $date,"title"=>$record->term,
                                "y" => $record->count,"label" => $record->companyName ? $record->companyName : "nil"));
                        }
                    }
                }
                $x += 1;
                $we["title"]= $query->term;
                $we["array"] = array($newArray);
                array_push($finaleArray,$we);
            }
            $data['test'] = $typeofQueries;
        $data["dataPoints"] = $finaleArray;
        return view("charts.index",$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Result::where('id',$id)->update(
            [
                "companyName"=> $request->companyName,
            ]
        );

        $data['title'] = "Requests";
        $data['result'] = DB::table('results')
            ->orderBy('created_at','desc')
            ->where("companyName","=","")
            ->first();
        $data['count'] = DB::table('results')
            ->orderBy('created_at','desc')
            ->where("companyName","=","")
            ->count();

        return view("results.edit ",$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Request::where('id',$id)->delete();
        //Request::where('requestID', $id)->delete();

        return Redirect::to('requests')->with('success','Requests deleted successfully');
    }

}
