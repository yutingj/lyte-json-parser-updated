<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = [
        'id',
        'url',
        'method',
        'retryCount',
        'resultsTotal',
        'hasNextPage',
        'durationSecs',
        'statusCode',
        'errorMessages',
    ];

    public $incrementing = false;

    protected $visible = [
        'id',
        'url',
        'method',
        'retryCount',
        'resultsTotal',
        'hasNextPage',
        'durationSecs',
        'statusCode',
        'errorMessages',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $primaryKey = 'id';
}
