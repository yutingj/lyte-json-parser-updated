<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'title',
        'displayURL',
        'description',
        'companyName',
        'date',
        'requestID',
    ];

    public $incrementing = true;

    protected $visible = [
        'id',
        'title',
        'displayURL',
        'description',
        'companyName',
        'date',
        'requestID',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $primaryKey = 'id';
}
