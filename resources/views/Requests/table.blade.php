@section('content')

<div class="buttonRow">
    <a href="" class="btn btn-success mb-2">Add</a>
</div>

<div class="">
    <table class="table table-bordered" id="laravel_crud">
        <thead>
        <tr>
            <th>Id</th>
            <th>URL</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th colspan="1" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($requests as $request)
        <tr>
            <td>{{ $request->id }}</td>
            <td>{{ $request->url }}</td>
            <td>{{ $request->created_at }}</td>
            <td>{{ $request->updated_at }}</td>
            <td class="">
                <div>
                    <a href="{{ route('requests.edit',$request->id)}}" style="display:inline-block" class="btn btn-primary">Edit</a>
                    <form action="{{ route('requests.destroy', $request->id)}}" style="display:inline-block"  method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </div>
            </td>
        </tr>
        @endforeach

        @if(count($requests) < 1)
        <tr>
            <td colspan="10" class="text-center">There are no request available yet!</td>
            </td>
        </tr>
        @endif
        </tbody>
    </table>
    <div style="display:flex;justify-content:center">
        {{$requests->links()}}
    </div>
</div>
@endsection
