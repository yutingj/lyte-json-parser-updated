<div>
<nav class="navbar navbar-expand-lg " color-on-scroll="500">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">LyteJP</a>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="nav navbar-nav mr-auto">
                <li class="nav-item">
                   <a href="/#" class="nav-link">
                       Home
                   </a>
                </li>
                <li class="nav-item">
                   <a href="{{ route('parseJson.index') }}" class="nav-link">
                       Parse Json
                   </a>
                </li>
                <li class="nav-item">
                   <a href="{{ route('requests.index') }} " class="nav-link">
                       View data
                   </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('updateEmpty') }} " class="nav-link">
                        Update company's name
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('charts') }} " class="nav-link">
                        Charts
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>
