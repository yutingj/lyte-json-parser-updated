@extends('layouts.default')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">

<hr />

<form action="{{ route('parseJson.store') }}" method="POST" name="add_request">
    {{ csrf_field() }}
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <strong>Json String</strong>
                    <textarea class="form-control" id="jsonString" name="jsonString" rows="7" placeholder="Enter Json String"></textarea>
                </div>

            </div>
            <div class="col">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Store</button>
            </div>
        </div>
</form>
@endsection
