@section('content')
    <script>
        window.onload = function () {
            var js_array = <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>;
            for(var i=0; i< js_array.length;i++){
                console.log(js_array[i]);
                new CanvasJS.Chart(js_array[i]["title"], {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light1", // "light1", "light2", "dark1", "dark2"
                    title:{
                        text: js_array[i]["title"]
                    },
                    toolTip:{
                        content:"{label} {x}, Count: {y}" ,
                    },
                    axisX:{
                        title: "Month - Year",
                        valueFormatString: "MMM-YY"
                    },
                    axisY:{
                        title: "Number of occurences",
                    },
                    data: [{
                        type: "scatter", //change type to bar, line, area, pie, etc
                        //indexLabel: "{y}", //Shows y value on all Data Points
                        indexLabelFontColor: "#5A5757",
                        indexLabelPlacement: "outside",
                        xValueType: "dateTime",
                        dataPoints: js_array[i]["array"][0]
                    }]
                }).render();
            }
        }
    </script>
</head>
<body>
@foreach ($dataPoints as $page)
<div id="{{$page['title']}}" style="height: 370px; width: 100%;"></div>

@endforeach
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
