@extends('layouts.default')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">

<div class="buttonRow">
    <div>
        {{ $count }} left.
    </div>
    <div class="text-right">
        <a href="{{ route('requests.index') }}" class="btn btn-danger mb-2">Go Back</a>
    </div>
</div>
<hr />
@if(!empty($result->id))
<form action="{{ route('requests.update', $result->id) }}" method="POST" name="update_result">
    @method('PATCH')
    {{ csrf_field() }}
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <strong>Title</strong>
                    <textarea rows="10"type="text" id="descriptionBox" name="title" readonly="readonly" class="form-control">{{$result->title}}
                    </textarea>
                </div>
                <div class="form-group">
                    <strong>Description</strong>
                    <textarea rows="10"type="text" id="descriptionBox" name="descriptionBox" readonly="readonly" class="form-control">{{$result->description}}
                    </textarea>
                </div>
                <div class="form-group">
                    <strong>Company Name</strong>
                    <input type="text" id="companyName" class="form-control" name="companyName" placeholder="Enter Company Name:">
                    <span class="text-danger">{{ $errors->first('invoiceDate') }}</span>
                </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" id="buttonActivate" class="btn btn-primary">Highlighted Text</button>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('requests.index') }}" class="btn btn-danger">Cancel</a>
            </div>
        </div>
</form>
@endif
@endsection
