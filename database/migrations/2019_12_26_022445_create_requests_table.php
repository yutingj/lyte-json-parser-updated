<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->string('id',50);
            $table->string('url',500);
            $table->string('method', 10);
            $table->integer('retryCount');
            $table->integer('resultsTotal');
            $table->boolean('hasNextPage');
            $table->decimal('durationSecs', 20,2);
            $table->string('statusCode', 10);
            $table->string('errorMessages', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
