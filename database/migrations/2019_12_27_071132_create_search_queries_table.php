<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_queries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('term',50)->nullable();
            $table->bigInteger('page')->nullable();
            $table->string('countryCode', 10)->nullable();
            $table->string('domain', 50)->nullable();
            $table->string('requestID', 50)->nullable();
            $table->bigInteger('resultsPerPage')->nullable();
            $table->string('device',50)->nullable();
            $table->string('locationUule', 50)->nullable();
            $table->string('type',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_queries');
    }
}
